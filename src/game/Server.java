package game;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Server {

    public static final int size = 20;
    public static final int scene_height = size * 20 + 100;
    public static final int scene_width = size * 20 + 200;

    public static Image image_floor;
    public static Image image_wall;
    public static Image hero_right, hero_left, hero_up, hero_down;

    // public static Player me = new Player();
    public static List<Player> players = new ArrayList<>();
    public static List<Thread> serverthreads = new ArrayList<>();

    private TextArea scoreList;

    public static String[] board = { // 20x20
            "wwwwwwwwwwwwwwwwwwww", "w        ww        w", "w w  w  www w  w  ww", "w w  w   ww w  w  ww",
            "w  w               w", "w w w w w w w  w  ww", "w w     www w  w  ww", "w w     w w w  w  ww",
            "w   w w  w  w  w   w", "w     w  w  w  w   w", "w ww ww        w  ww", "w  w w    w    w  ww",
            "w        ww w  w  ww", "w         w w  w  ww", "w        w     w  ww", "w  w              ww",
            "w  w www  w w  ww ww", "w w      ww w     ww", "w   w   ww  w      w", "wwwwwwwwwwwwwwwwwwww" };

    public static void main(String[] args) throws Exception {

        int playerCount = 0;

        ServerSocket welcomeSocket = new ServerSocket(6789);
        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
            // playerName = inFromClient.readLine();
            // if (playerCount > 0) {
            (new ServerThread1(connectionSocket, 1)).start();
            // playerCount++;
            // Thread thread1 = new ServerThread1(connectionSocket, 1);
            // thread1.start();
            // serverthreads.add(thread1);
        }
        // if (playerCount > 1) {
        // (new ServerThread2(connectionSocket, 2)).start();
    }

    // }
    // }

    public static void playerMovedPoint(int delta_x, int delta_y, String direction, Player me) {

        me.direction = direction;
        int x = me.getXpos(), y = me.getYpos();

        // if (board[y + delta_y].charAt(x + delta_x) == 'w') {
        // me.addPoints(-1);
        // } else {
        // Player p = getPlayerAt(x + delta_x, y + delta_y);
        // if (p != null) {
        // me.addPoints(10);
        // p.addPoints(-10);
        // } else {
        // me.addPoints(1);
        // final int x1 = x;
        // final int y1 = y;
        //
        // x += delta_x;
        // y += delta_y;
        // final int x2 = x;
        // final int y2 = y;

        // for(int i = 0; i < players.size(); i++) {
        // players.get(i).setXpos(players.get(i).getXpos());
        // players.get(i).setYpos(players.get(i).getYpos());
        //
        //

        switch (direction) {
        case "up":
            if (board[me.getYpos() - 1].charAt(me.getXpos()) == 'w') {
                me.setPoint(me.getPoint() - 1);
            } else {
                me.setXpos(me.getXpos());
                me.setYpos(me.getYpos() - 1);
                me.setPoint(me.getPoint() + 1);
                me.setDirection("up");
            }
            break;
        case "down":
            if (board[me.getYpos() + 1].charAt(me.getXpos()) == 'w') {
                me.setPoint(me.getPoint() - 1);
            } else {
                me.setXpos(me.getXpos());
                me.setYpos(me.getYpos() + 1);
                me.setPoint(me.getPoint() + 1);
                me.setDirection("down");
            }
            break;
        case "left":
            if (board[me.getYpos()].charAt(me.getXpos() - 1) == 'w') {
                me.setPoint(me.getPoint() - 1);
            } else {
                me.setXpos(me.getXpos() - 1);
                me.setYpos(me.getYpos());
                me.setPoint(me.getPoint() + 1);
                me.setDirection("left");
            }

            break;
        case "right":
            if (board[me.getYpos()].charAt(me.getXpos() + 1) == 'w') {
                me.setPoint(me.getPoint() - 1);
            } else {
                me.setXpos(me.getXpos() + 1);
                me.setYpos(me.getYpos());
                me.setPoint(me.getPoint() + 1);
                me.setDirection("right");
            }
            break;
        default:
            break;
        }
    }
    // }
    // }

    // }

    public static Player getPlayerAt(int x, int y) {
        for (Player p : players) {
            if (p.getXpos() == x && p.getYpos() == y) {
                return p;
            }
        }
        return null;
    }

}
