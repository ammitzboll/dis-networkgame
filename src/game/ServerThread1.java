package game;

import java.net.*;
import java.util.ArrayList;
import java.io.*;

public class ServerThread1 extends Thread {
    Socket connSocket;

    Player me;
    int playerNumber;
    int xPos = 0;
    int yPos = 0;

    public ServerThread1(Socket connSocket, int playerNumber) {
        this.connSocket = connSocket;
        this.playerNumber = playerNumber;

    }

    @Override
    public void run() {
        try {
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connSocket.getOutputStream());

            String playerName = inFromClient.readLine();
            // if(p1 == null) {
            me = new Player(playerName, 9, 4, "up", 0);
            Server.players.add(me);
            String sentence;
            sentence = me.getName() + " " + me.getXpos() + " " + me.getYpos() + " " + me.getDirection() + " "
                    + me.getPoint();
            outToClient.writeBytes(sentence + "\n");

            // }
            while (!Server.players.isEmpty()) {
                // String move = inFromClient.readLine();

                String[] koordinat = inFromClient.readLine().split(" ");
                String direction = koordinat[0];
                me.setDirection(direction);
                xPos = me.getXpos();
                yPos = me.getYpos();
                System.out.println(direction);
                Server.playerMovedPoint(xPos, yPos, direction, me);
                for (Player p : Server.players) {
                    sentence = p.getName() + " " + p.getXpos() + " " + p.getYpos() + " " + p.getDirection() + " "
                            + p.getPoint();
                    sentence += "!";
                    outToClient.writeBytes(sentence + "\n");
                }

                // int xKoordinat = Integer.parseInt(koordinat[0]);
                // int yKoordinat = Integer.parseInt(koordinat[1]);
                //
                // Server.players.get(0).setXpos(xKoordinat);
                // Server.players.get(0).setYpos(yKoordinat);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
