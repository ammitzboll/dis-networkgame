package game;

import java.io.BufferedReader;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;

public class ClientThread extends Thread {

    BufferedReader inFromServer;
    BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
    DataOutputStream outToServer;
    String sentence;
    Player me = new Player();
    String name;

    public ClientThread(BufferedReader inFromServer, DataOutputStream outToServer) {
        this.inFromServer = inFromServer;
        this.outToServer = outToServer;
    }

    @Override
    public void run() {
        if (me.name == null) { // kig lige lidt mere på 'me'
            System.out.println("Please enter your name");

            try {
                sentence = inFromUser.readLine();
                outToServer.writeBytes(sentence + "\n");
                String a = inFromServer.readLine();
                System.out.println("Test " + a);
                String[] findName = a.split(" ");
                name = findName[0];
                int posX = Integer.parseInt(findName[1]);
                int posY = Integer.parseInt(findName[2]);
                Platform.runLater(() -> {
                    Main.fields[posX][posY].setGraphic(new ImageView(Main.hero_up));
                    me.setName(name);
                    me.setXpos(posX);
                    me.setYpos(posY);
                    me.setDirection("hero_up");
                });
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        while (true) {

            recievePlayers(Server.players, inFromServer);
            // outToServer.writeBytes(me.getXpos() + " " + me.getYpos());
            // opdater grænseflade

            for (Player p : Server.players) {
                Platform.runLater(() -> {
                    if (p.direction.equals("right")) {
                        Main.fields[p.xpos][p.ypos].setGraphic(new ImageView(Main.hero_right));
                        if (Main.board[p.getYpos()].charAt(p.getXpos() - 1) == 'w') {

                        } else {
                            Main.fields[p.xpos - 1][p.ypos].setGraphic(new ImageView(Main.image_floor));
                        }
                    }
                    if (p.direction.equals("left")) {
                        Main.fields[p.xpos][p.ypos].setGraphic(new ImageView(Main.hero_left));
                        if (Main.board[p.getYpos()].charAt(p.getXpos() + 1) == 'w') {

                        } else {
                            Main.fields[p.xpos + 1][p.ypos].setGraphic(new ImageView(Main.image_floor));
                        }
                    }
                    if (p.direction.equals("up")) {
                        Main.fields[p.xpos][p.ypos].setGraphic(new ImageView(Main.hero_up));
                        if (Main.board[p.getYpos() + 1].charAt(p.getXpos()) == 'w') {

                        } else {
                            Main.fields[p.xpos][p.ypos + 1].setGraphic(new ImageView(Main.image_floor));
                        }
                    }
                    if (p.direction.equals("down")) {
                        Main.fields[p.xpos][p.ypos].setGraphic(new ImageView(Main.hero_down));
                        if (Main.board[p.getYpos() - 1].charAt(p.getXpos()) == 'w') {

                        } else {
                            Main.fields[p.xpos][p.ypos - 1].setGraphic(new ImageView(Main.image_floor));
                        }
                    }
                });
                Platform.runLater(() -> {
                    Main.scoreList.setText(Main.getScoreList());
                    System.out.println(me.getPoint());
                });

            }

        }
    }

    public void recievePlayers(List<Player> players, BufferedReader instream) {
        String[] input = null;
        try {
            input = instream.readLine().split("!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Player p : players) {
            Main.fields[p.xpos][p.ypos].setGraphic(new ImageView(Main.image_floor));
        }
        players.clear();
        for (int i = 0; i < input.length; i++) {
            String[] asd = input[i].split(" ");
            Player p = new Player(asd[0], Integer.parseInt(asd[1]), Integer.parseInt(asd[2]), asd[3],
                    Integer.parseInt(asd[4]));
            players.add(p);
        }
    }
}
